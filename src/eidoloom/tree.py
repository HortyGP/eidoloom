import ast
# import fnmatch
import os

tree = r'''
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, random


class Joias:
    """Implementacao parcial do jogo "Jewels"."""

    def __init__(self):
        """Inicializa o jogo."""
        self.tab = [[0 for j in range(8)] for i in range(8)]
        self.preenche_tabuleiro()

    def preenche_vazias(self):
        """Preenche as casas vazias (preenchidas com zeros) do tabuleiro."""
        for i in range(8):
            for j in range(8):
                if self.tab[i][j] == 0: self.tab[i][j] = random.randint(1, 7)

    def preenche_tabuleiro(self):
        """Preenche o tabuleiro, assegurando que nÃ£o hÃ¡ triplas."""
        self.preenche_vazias()
        while self.limpa_triplas():
            self.atualiza_tabuleiro()

    def atualiza_tabuleiro(self):
        """Atualiza o tabuleiro apÃ³s um movimento."""
        self.compacta_colunas()
        self.preenche_vazias()

    def limpa_triplas(self):
        """Limpa as triplas do tabuleiro, substituindo as peÃ§as por zeros.
        Retorna True se houve triplas a limpar ou False, caso contrÃ¡rio."""
        posicoes = self.procura_triplas()
        for i, j in posicoes: self.tab[i][j] = 0
        return len(posicoes)

    def swap(self, a, b, c, d):
        """Troca as posiÃ§Ãµes self.tab[a][b] com self.tab[c][d]."""
        self.tab[a][b], self.tab[c][d] = self.tab[c][d], self.tab[a][b]

    def compacta_colunas(self):
        """Compacta as colunas do tabuleiro fazendo com que os zeros do tabuleiro
        se concentrem nas linhas de nÃºmero mais alto."""
        for j in range(8):
            for i in range(7, 0, -1):
                for k in range(i):
                    if self.tab[k][j] == 0:
                        self.swap(k, j, k + 1, j)

    def __repr__(self):
        """Retorna uma string com uma representaÃ§Ã£o do tabuleiro."""
        return "0 1 2 3 4 5 6 7\n\n" + \
               "\n".join([" ".join([" .+*$%#@"[self.tab[i][j]] \
                                    for j in range(8)])
                          for i in range(7, -1, -1)]) + "\n"
'''
ntree = tree.replace(r'\n', ' ')
tree = ast.parse(ntree)
# print(ast.dump(tree))

# from spekuloom.util import Mont
# d = Mont().mont_symbol_pt()
# for k, v in d.items():
#     print(k, v)

TOKENS = dict(
    Module='▲', Assign='◉', Name='▲', Store='▣', List='▣', Str='◱', Load='◬', ClassDef='◪',
    FunctionDef='◨', arguments='▵', arg='▵', Expr='◉', ListComp='◈', comprehension='◈', Call='●',
    Num='◳', For='◠', Attribute='◭', Import='◡', alias='◡', ImportFrom='◡', Dict='▣', With='◡',
    ExceptHandler='◔', Raise='◔', Pass='◐', Return='●', Global='◭', Nonlocal='◭', IfExp='▬', Compare='◎',
    Gt='▱', UnaryOp='◉', USub='▲', While='◠', In='◎', AugAssign='◉', Add='■', If='▬', Continue='◐', GtE='▰',
    Break='◐', Is='◎', NameConstant='▲', DictComp='◈', JoinedStr='◲', FormattedValue='◲', Tuple='▣',
    GeneratorExp='◈', Yield='●', BoolOp='◻', Or='◻', Lt='▱', And='◻', Eq='◻', LtE='▰', Not='◻',
    Assert='◕', Delete='◕', Del='◕', Lambda='◨', BinOp='■', Mult='■', Pow='■', Sub='■', Div='■', FloorDiv='■', Mod='■'
)


class NodeVisitor(ast.NodeVisitor):
    def __init__(self):

        self.gram = []

    def visit_Call(self, tree_node):
        # print(dir(tree_node.func))
        # print('●({})'.format(tree_node.func.id))
        self.gram.append("(")
        self.visit(tree_node.func)
        self.gram[-1] = self.gram[-1].rstrip(",")
        self.gram.append(")●,")

    def visit_Str(self, tree_node):
        # print('◱{}'.format(tree_node.s))
        self.gram.append("◱,")

    def visit_Name(self, tree_node):
        # print('{}'.format(tree_node.id))
        self.gram.append("△,")

    def _visit_Assign(self, tree_node):
        # print('◉({})'.format(list(repr(tree_node.targets[0]))))
        # print('◉({})'.format(list(tree_node.targets)[0].id))
        self.gram.append("(")
        self.visit(tree_node.value)
        self.gram[-1] = self.gram[-1].rstrip(",")
        self.gram.append(")◉,")

    def visit_For(self, tree_node):
        # print('◠({})'.format(tree_node.target.id))
        self.gram.append("(")
        [self.visit(node) for node in tree_node.body]
        self.gram[-1] = self.gram[-1].rstrip(",")
        self.gram.append(")◠,")

    def _visit_Attribute(self, tree_node):
        # print('◭({})'.format(tree_node.value))
        self.gram.append("(")
        [self.visit(node) for node in tree_node.args]
        self.gram[-1] = self.gram[-1].rstrip(",")
        self.gram.append(")◭,")

    def visit_ClassDef(self, tree_node):
        # print('◪({})'.format(tree_node.name))
        self.gram.append("(")
        [self.visit(node) for node in tree_node.body]
        self.gram[-1] = self.gram[-1].rstrip(",")
        self.gram.append(")◪,")

    def visit_FunctionDef(self, tree_node):
        # print('◨({})'.format(tree_node.name))
        self.gram.append("(")
        [self.visit(node) for node in tree_node.body]
        self.gram[-1] = self.gram[-1].rstrip(",")
        self.gram.append(")◨,")


class GenV(NodeVisitor):
    def __init__(self):
        super().__init__()
        self.corpus = f"{os.path.dirname(__file__)}/../EidoloomCorpora"
        self.tokens = {}
        # self.gram = []

    def generic_visit(self, node):
        token_name = type(node).__name__
        self.tokens[token_name] = "\033[1;30m" + u"\u25B2" + "\033[1;0m"
        if token_name in TOKENS:
            self.tokens[token_name] = TOKENS[token_name]
            self.gram += [f"{TOKENS[token_name]},"]
        ast.NodeVisitor.generic_visit(self, node)

    def tokenize(self, text):
        self.visit(ast.parse(text))
        return list(self.gram)

    def print_tokens(self):

        # for tok in self.tokens:
        #     print("{}='{}',".format(tok, self.tokens[tok]), end=' ')
        # print("toks")

        for tok in self.gram:
            print("{}".format(tok), end='')
        self.tokens = {}
        self.gram = ""

    def visit_corpus(self):
        print("visit_corpus", self.corpus)
        for root, direc, files in os.walk(self.corpus):
            path = root.split(os.sep)
            print(os.path.basename(root))
            for file in files:
                filepath = os.path.join(root, file)
                with open(filepath, "r") as txtfile:
                    source_text = txtfile.read()
                    # source_text = r'{}'.format(txtfile.read())
                    # source_text = source_text.replace(r'\n', ' ')
                    astree = ast.parse(source_text)
                    print('\n', len(path) * '---', file, source_text[:20])
                    self.visit(astree)
                    self.print_tokens()
            # for sample in self.corpus:
            # self.visit(sample)

from ete3 import Tree, TreeStyle, PhyloTree, TextFace, add_face_to_node, AttrFace, faces

t = PhyloTree("(((seqA,seqB)Internal1,seqC)Internal2,seqD);", format=1)
ts = TreeStyle()
ts.show_leaf_name = False


def my_layout(node):
        # F = AttrFace("name", fsize=30)
        F = TextFace(node.name,  fsize=8)
        faces.add_face_to_node(F, node, 1, position="float")
        # faces.add_face_to_node(F, node, column=0, position="branch-top")
        # if node.is_leaf():
        #         #seq_face = SeqMotifFace(node.sequence, seqtype='aa', seq_format='seq')
        #         add_face_to_node(seq_face, node, column=0, position='aligned')
ts.layout_fn = my_layout
# t.show(tree_style=ts)
if __name__ == "__main__":

    gv = GenV()
    gv.visit(tree)
    # gv .print_tokens()
    gram = "({});".format("".join(gv.gram).rstrip(","))
    print(gram)
    t = PhyloTree(gram, format=1)
    ts = TreeStyle()
    ts.mode = "c"
    ts.show_leaf_name = False
    ts.layout_fn = my_layout
    t.show(tree_style=ts)

    # gv.visit_corpus()
    from ete3 import Tree
    #print(Tree("(a,b,(c,(d,e)F:2)I:1)Root;", format=1, quoted_node_names=1))
    # t.show(tree_style=ts)
    """

    for tok in gv.tokens:
        print("{}='{}',".format(tok, gv.tokens[tok]), end=' ')
    print("toks")

    for tok in gv.gram:
        print("{}".format(tok), end='')

encondes:
noun,▲  False True None▲
name △
numeral ◳
textual ◱
article,
adjective,◭
class def lambda◪
global nonlocal◭
verb, ●   yield return●
continue break pass◐
try raise◔
del assert import◕
adverb,○   finally except○
in is◎
preposition,◡  from with as◡
while for◠
conjuction,▬  if else elif▬
or and not◻
"""
