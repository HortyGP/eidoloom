import pep8
import os

#path = "/home/hortencia/PycharmProjects/eidoloom/src/eidoloom/Experimento01"
path = "/home/hortencia/PycharmProjects/eidoloom/src/EidoloomCorpora/transitorio"
for root, dirs, files in os.walk(path):  # parse through file list in the current directory
    for filename in files:
        print(os.path.join(root, filename))
        code = os.path.join(root, filename)
        with open(code) as thecode:
            thecode = thecode.read()
            fchecker = pep8.Checker(lines=thecode.splitlines(True), show_source=True)
            errors = fchecker.check_all()
            print("PEP8 error count: {}".format(errors))

