import ast
# import fnmatch
import os
import logging
log = logging.getLogger('dev')
log.setLevel(logging.CRITICAL)
tree = r'''
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, random


class Joias:
    """Implementacao parcial do jogo "Jewels"."""

    def __init__(self):
        """Inicializa o jogo."""
        self.tab = [[0 for j in range(8)] for i in range(8)]
        self.preenche_tabuleiro()

    def preenche_vazias(self):
        """Preenche as casas vazias (preenchidas com zeros) do tabuleiro."""
        for i in range(8):
            for j in range(8):
                if self.tab[i][j] == 0: self.tab[i][j] = random.randint(1, 7)

    def preenche_tabuleiro(self):
        """Preenche o tabuleiro, assegurando que nÃ£o hÃ¡ triplas."""
        self.preenche_vazias()
        while self.limpa_triplas():
            self.atualiza_tabuleiro()

    def atualiza_tabuleiro(self):
        """Atualiza o tabuleiro apÃ³s um movimento."""
        self.compacta_colunas()
        self.preenche_vazias()

    def procura_triplas(self):
        """Procura no tabuleiro trÃªs ou mais peÃ§as seguidas iguais em linhas ou
        colunas. Retorna uma lista de posiÃ§Ãµes onde tais peÃ§as se encontram. Uma
        posiÃ§Ã£o Ã© um par (linha,coluna) onde linha e coluna sÃ£o Ã­ndices de 0 a 7."""
        triplas = []
        for i in range(8):
            for j in range(6):
                if self.tab[i][j] == self.tab[i][j + 1] and \
                                self.tab[i][j] == self.tab[i][j + 2]:
                    triplas += [(i, j), (i, j + 1), (i, j + 2)]
        for j in range(8):
            for i in range(6):
                if self.tab[i][j] == self.tab[i + 1][j] and \
                                self.tab[i][j] == self.tab[i + 2][j]:
                    triplas += [(i, j), (i + 1, j), (i + 2, j)]
        return triplas

    def limpa_triplas(self):
        """Limpa as triplas do tabuleiro, substituindo as peÃ§as por zeros.
        Retorna True se houve triplas a limpar ou False, caso contrÃ¡rio."""
        posicoes = self.procura_triplas()
        for i, j in posicoes: self.tab[i][j] = 0
        return len(posicoes)

    def swap(self, a, b, c, d):
        """Troca as posiÃ§Ãµes self.tab[a][b] com self.tab[c][d]."""
        self.tab[a][b], self.tab[c][d] = self.tab[c][d], self.tab[a][b]

    def compacta_colunas(self):
        """Compacta as colunas do tabuleiro fazendo com que os zeros do tabuleiro
        se concentrem nas linhas de nÃºmero mais alto."""
        for j in range(8):
            for i in range(7, 0, -1):
                for k in range(i):
                    if self.tab[k][j] == 0:
                        self.swap(k, j, k + 1, j)

    def __repr__(self):
        """Retorna uma string com uma representaÃ§Ã£o do tabuleiro."""
        return "0 1 2 3 4 5 6 7\n\n" + \
               "\n".join([" ".join([" .+*$%#@"[self.tab[i][j]] \
                                    for j in range(8)])
                          for i in range(7, -1, -1)]) + "\n"


def mainloop():
    global total
    while True:
        try:
            if (sys.hexversion > 0x03000000):
                (i, j) = tuple(int(x.strip()) for x in input("Entre a posicao (i,j): ").split(','))
            else:
                (i, j) = input("Entre a posicao (i,j): ")
            i = min(max(0, i), 7)
            j = min(max(0, j), 7)
            pos = input("Entre a direcao (up, down, left, right): ")
            if pos == 'left' and j > 0:
                l = j - 1
                k = i
            elif pos == 'up' and i < 7:
                k = i + 1
                l = j
            elif pos == 'down' and i > 0:
                k = i - 1
                l = j
            elif pos == 'right' and j < 7:
                l = j + 1
                k = i
            else:
                raise ValueError("Direcao Invalida")
        except (NameError, TypeError) as e:
            print("Tente de novo (%s):" % e)
            print("posições: 0..7, 0..7")
        except SyntaxError:
            print("Total de jóias = ", total)
            sys.exit("Fim")
        except KeyboardInterrupt as e:
            sys.exit(e)
        else:
            break

    jew.swap(i, j, k, l)
    nt = jew.limpa_triplas()
    if nt > 0:  # um Ãºnico movimento pode criar duas triplas
        jew.atualiza_tabuleiro()
        total += nt / 3
    else:
        jew.swap(k, l, i, j)
        raise ValueError("Movimento Inválido")


def main():
    total = 0
    jew = Joias()
    print(jew)
    while True:
        try:
            mainloop()
        except ValueError as e:
            print(e)
        print(jew)


if __name__ == "__main__":
    while True:
        main()
'''
ntree = tree.replace(r'\n', ' ')
tree = ast.parse(ntree)
# print(ast.dump(tree))

# from spekuloom.util import Mont
# d = Mont().mont_symbol_pt()
# for k, v in d.items():
#     print(k, v)

TOKENS = dict(
    Module='▲', Assign='◉', Name='▲', Store='▣', List='▣', Str='◱', Load='◬', ClassDef='◪',
    FunctionDef='◨', arguments='▵', arg='▵', Expr='◉', ListComp='◈', comprehension='◈', Call='●',
    Num='◳', For='◠', Attribute='◭', Import='◡', alias='◡', ImportFrom='◡', Dict='▣', With='◡',
    ExceptHandler='◔', Raise='◔', Pass='◐', Return='●', Global='◭', Nonlocal='◭', IfExp='▬', Compare='◎',
    Gt='▱', UnaryOp='◉', USub='▲', While='◠', In='◎', AugAssign='◉', Add='■', If='▬', Continue='◐', GtE='▰',
    Break='◐', Is='◎', NameConstant='▲', DictComp='◈', JoinedStr='◲', FormattedValue='◲', Tuple='▣',
    GeneratorExp='◈', Yield='●', BoolOp='◻', Or='◻', Lt='▱', And='◻', Eq='◻', LtE='▰', Not='◻',
    Assert='◕', Delete='◕', Del='◕', Lambda='◨', BinOp='■', Mult='■', Pow='■', Sub='■', Div='■', FloorDiv='■', Mod='■'
)


class Code:
    CODES = {}
    WINDOW_SIZE = 3
    WINDOW_COVER = 3
    PATTERNS = {}

    def __init__(self, astree, gram, kind):
        self.ast, self.gram, self.kind = astree, gram, kind
        self.patterns = {}

    def compute_patterns(self, norm=True, patamar=0.1):
        splits = [a for a in range(Code.WINDOW_SIZE)]
        grams = [self.gram[off:] for off in splits]
        pattern = ["".join(symbol) for symbol in zip(*grams)]
        self.patterns = {pat: 0 for pat in pattern}
        [self.patterns.update({pat: self.patterns[pat] + 1}) for pat in pattern]
        all_patterns = sum(self.patterns.values())
        self.patterns = {pat: 1000.0*value/all_patterns for pat, value in self.patterns.items()} if norm else self.patterns
        [Code.PATTERNS.update({pat: Code.PATTERNS.setdefault(pat, []) + [count]})
         for pat, count in self.patterns.items()]


class GenV(ast.NodeVisitor):
    def __init__(self):
        self.corpus = f"{os.path.dirname(__file__)}/../EidoloomCorpora"
        self.tokens = {}
        self.gram = ""

    def generic_visit(self, node):
        token_name = type(node).__name__
        self.tokens[token_name] = "\033[1;30m" + u"\u25B2" + "\033[1;0m"
        if token_name in TOKENS:
            self.tokens[token_name] = TOKENS[token_name]
            self.gram += TOKENS[token_name]
        ast.NodeVisitor.generic_visit(self, node)

    def tokenize(self, text):
        self.visit(ast.parse(text))
        return list(self.gram)

    def print_tokens(self):

        # for tok in self.tokens:
            # print("{}='{}',".format(tok, self.tokens[tok]), end=' ')
        # print("toks")

        for tok in self.gram:
            print("{}".format(tok), end='')
        self.tokens = {}
        self.gram = ""

    def visit_corpus(self):
        log.debug("visit_corpus", self.corpus)
        for root, direc, files in os.walk(self.corpus):
            path = root.split(os.sep)
            log.debug(os.path.basename(root))
            kind = os.path.basename(root)
            for file in files:
                filepath = os.path.join(root, file)
                with open(filepath, "r") as txtfile:
                    source_text = txtfile.read()
                    # source_text = r'{}'.format(txtfile.read())
                    # source_text = source_text.replace(r'\n', ' ')
                    astree = ast.parse(source_text)
                    log.debug('\n', len(path) * '---', file, source_text[:20])
                    self.visit(astree)
                    Code.CODES[file[:4].lower()] = Code(astree, self.gram, kind)
                    # self.print_tokens()
            # for sample in self.corpus:
            # self.visit(sample)


class NodeVisitor(ast.NodeVisitor):
    def visit_Call(self, tree_node):
        print('●{}'.format(tree_node.func.id))
        self.visit(tree_node.func)

    def visit_Str(self, tree_node):
        print('◱{}'.format(tree_node.s))

    def visit_Name(self, tree_node):
        print('△{}'.format(tree_node.id))

    def visit_Assign(self, tree_node):
        print('◉{}'.format(list(tree_node.targets)[0].id))
        self.visit(tree_node.value)

    def visit_For(self, tree_node):
        print('◠{}'.format(tree_node.target.id))
        [self.visit(node) for node in tree_node.body]

    def visit_Attribute(self, tree_node):
        print('◭{}'.format(tree_node.value.s))
        [self.visit(node) for node in tree_node.args]

    def visit_ClassDef(self, tree_node):
        print('◪{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_FunctionDef(self, tree_node):
        print('◨{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]


if __name__ == "__main__":

    gv = GenV()
    # gv.visit(tree)
    gv.visit_corpus()
    """

    for tok in gv.tokens:
        print("{}='{}',".format(tok, gv.tokens[tok]), end=' ')
    print("toks")

    for tok in gv.gram:
        print("{}".format(tok), end='')

encondes:
noun,▲  False True None▲
name △
numeral ◳
textual ◱
article,
adjective,◭
class def lambda◪
global nonlocal◭
verb, ●   yield return●
continue break pass◐
try raise◔
del assert import◕
adverb,○   finally except○
in is◎
preposition,◡  from with as◡
while for◠
conjuction,▬  if else elif▬
or and not◻
"""
