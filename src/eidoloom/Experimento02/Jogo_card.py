# Card.py
# This class represents a Card used for card games.

# Card.py
# This class represents a Deck of cards used for card games.
# from Card import Card

import random


class Deck(object):
    def __init__(self):
        self.arr = []

    def create_default_deck(self):
        suits = ["Hearts", "Diamonds", "Spades", "Clubs"]
        idx = 0
        for i in range(1, 5, 1):
            for k in range(2, 15, 1):
                c = Card(k, suits[i - 1], "Blackjack")
                self.arr.append(c)

    def print_deck(self):
        print
        "Is this working?"
        for c in self.arr:
            c.print_card()

    def shuffle(self):
        for i in range(len(self.arr)):
            rand = int(random.random() * (len(self.arr)))
            holder = self.arr[i]
            self.arr[i] = self.arr[rand]
            self.arr[rand] = holder

    def deal(self, num_cards, player=Player("")):
        player.give_cards(self.arr[len(self.arr) - num_cards:])
        del (self.arr[len(self.arr) - num_cards:])

    def get_top_card(self):
        return self.arr.pop(-1)

    def size(self):
        return len(self.arr)

    def get(self, idx):
        return self.arr(idx)

    def add(self, cards):
        for c in cards:
            self.arr.append(c);


class Card(object):
    def __init__(self, value, suit, game):
        self.value = value
        self.suit = suit
        self.game = game
        self.cName = self.set_name(value, suit)
        if game == "Blackjack":
            ace = False
            if value > 10:  # Workaround because elif isn't working correctly
                if value == 14:
                    ace = True
                self.value = 10
            if ace:
                self.value += 1

    def set_name(self, value, suit):
        if value > 10:
            arr = ["Jack", "Queen", "King", "Ace"]
            return arr[value % 10 - 1] + " of " + suit
        return str(value) + " of " + suit

    def get_name(self):
        return self.cName

    def get_val(self):
        return self.value

    def str_val(self):
        print("value of " + self.get_name() + " is " + str(self.val))


# Player.py
# This class represents a player of a game.
# from Card import Card
class Player(object):
    def __init__(self, name):
        self.name = name
        self.hand = []

    def give_cards(self, cards):
        for card in cards:
            self.hand.append(card)

    def get_name(self):
        return self.name

    def get_hand(self):
        return self.hand

    def get_hand_size(self):
        return len(self.hand)

    def return_hand(self):
        temp = []
        for c in self.hand:
            temp.append(c);
        self.hand[:] = []
        return temp;
