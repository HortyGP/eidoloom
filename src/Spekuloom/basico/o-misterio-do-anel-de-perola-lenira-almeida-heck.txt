LENIRA ALMEIDA HECK
(Júlia Vehuiah)

Autora: Lenira Almeida Heck
Inspirada por: Júlia Vehuiah
Ilustradora: Adriana Schnorr Dessoy
Editoração: Paulo Alexandre Fritsch

Tiragem: 1.000 exemplares
Copyright: Lenira Almeida Heck (Júlia Vehuiah)
Rua General Flores da Cunha, 84/102 - Bairro Florestal
95900-000 - Lajeado - RS
E-mail: lenira@univates.br - Fone: (51)3714-2472

H446m

Heck, Lenira Almeida
O mistério do anel de pérola / Lenira Almeida Heck. Lajeado: UNIVATES, 2008.
28 p. Il. Pb.
ISBN 978-85-98611-54-9
1. Literatura infantil I.Título
CDU: 82-93

Ficha catalográfica elaborada por: Claudia Carmem Baggio CRB 10/1830

LENIRA ALMEIDA HECK
(Júlia Vehuiah)

“A imaginação é um anjo que nos leva a viajar
pelo mundo mágico do faz de conta”.
Júlia Vehuiah

Agradecimentos:
A Deus, por mais esta obra.
A Júlia, pela vida.
A Vehuiah, pela inspiração.
A você, que se deixará conduzir ao mundo
fantasioso do Era uma vez... .

Ofereço esta obra

Dedico esta obra:
A todos os adultos com alma de criança e a todas as crianças que um
dia se tornarão adultos e que conservarão em si, a alma de criança.
À minha família- Roque, Aline, Davi, mini e meg.
À Professora Ivete, a Mirtô e ao Paulo – amigos com alma de criança.
À Dra. Priscila.
À Lise, Rose e Bárbara.
Aos brasileiros que, apesar de descontentes com tantas injustiças
sociais, não perdem a esperança de dias melhores. A todos, o meu
respeito e solidariedade.

3

E

ra uma vez duas irmãs

chamadas Cloé e Marli. Elas
moravam num vilarejo bem

distante; seu pai era o único
professor do lugar. A família
vivia nos fundos da escola.

4

Certo dia, a mãe de um colega e amiga da família resolveu
levar um presente para Cloé.
O professor foi à janela e...
- Cloé! Visita para você!
Ao chegar, dona Elizete entregou-lhe uma pequena caixa.
- Isto é para você. Cuide bem dela.

5
Dentro, estava uma pequena ave. Suas penas eram brancas
como a neve. Contente, Cloé agradeceu o presente, e saiu
para mostrá-lo à irmã.
- Marli! Marli! Olha o que ganhei!
- Aposto que esta coisa não tem nome nem certidão de
nascimento.

6

- Ih! É verdade! - concordou Cloé,
saindo em disparada.
- Dona Elizete! Dona Elizete! Por
favor, espere! A senhora esqueceu
de entregar a certidão de
nascimento da pintinha.

7
Conhecedora das brincadeiras de Marli, D. Elizete
respondeu:
- Volte e peça para Marli ir ao cartório registrá-la.
Cloé voltou e contou para a irmã o que D. Elizete havia dito.
- Então, vamos chamá-la... “Branquinha”.
E assim ficou.

8
Num piscar de olhos, Branquinha cresceu, engordou e se
transformou numa bela galinha, mansa e cacarejante.

Um belo dia, colocou o seu primeiro ovo, que tinha três
gemas. A alegria foi geral.

9
A mãe de Cloé aproveitou as gemas para fazer uma
deliciosa torta em homenagem à filha Marli, que estaria de
aniversário no dia seguinte.
O pai comprou-lhe um lindo presente.
Ao abri-lo, Marli pulou de alegria.
- Oh! Um anel de pérola.

10

Dias depois, as duas irmãs olhavam
Branquinha ciscar. Lá pelas tantas,
Marli tirou o anel do dedo e, num
descuido, ele escapou-lhe das mãos,
indo cair próximo à Branquinha.
Depressa, desceu para pegá-lo. Mas
não o encontrou.

10

11

E procura daqui, procura dali, nada
de o anel aparecer.
Aflita, Marli começou a chorar e
acusar Branquinha de tê-lo engolido.
A partir daquele instante, ela não
gostou mais da galinha.

12
Cloé, mais que depressa, pegou Branquinha e correu
para junto do pai.
Marli, soluçando, procurou a mãe e
contou tudo o que acontecera.
Muito tristes, lembraram da
lenda que dizia: quando as aves
engolem algum objeto de
ouro, três dias depois o
ouro derrete no ventre.
E nada puderam fazer.

13

O caso logo se espalhou pelo vilarejo. Algumas pessoas
começaram a seguir Branquinha por toda parte.
Após os três dias, alguém disse:
- Ih! Marli... Melhor perder as esperanças.
Mas Marli não perdia a esperança de encontrar o seu anel.

14
Uma tarde, a garota estava escondida atrás de uma moita,
abrindo o bico da galinha para ver se o anel estava
trancado em sua garganta. Outro dia, lá estava Marli
examinando o fiofó da galinha.

Cloé tirou Branquinha das mãos da irmã e saiu correndo.

15
Por causa desses e de outros episódios, Cloé não
desgrudou mais de Branquinha, mas espanto causou quando
começou a levá-la para a igreja.

16
O Padre não gostou nada daquela idéia maluca. Falando
baixinho, dizia:
- Valha-me, Deus! O que será que o bispo vai pensar, quando
souber que aqui neste lugar até as galinhas assistem às
missas? Com certeza, serei transferido ou considerado
louco. Só me faltava isso!

17
Mas o pior estava para acontecer. As outras crianças,
seguindo o exemplo de Cloé, também começaram a levar
animais para a igreja: galinhas, coelhos, gansos, preás,
cachorros, gatos, cabritos, tinha até uma porquinha
recém-nascida com um laço cor de rosa no pescoço.
Contam que alguém levou um papagaio cantador, que
cantava a música Mãezinha do Céu.

18
O Padre, por sua vez, rezava para que o Bispo não
aparecesse tão cedo por aquelas bandas.

Uma tarde, o religioso recebeu um
telefonema, avisando que o bispo e sua
comitiva estavam a caminho.
O santo homem começou a andar de um lado
para outro, muito preocupado. Aflito, correu
à casa dos fiéis para pedir que, enquanto
durasse a visita do bispo, ninguém levasse os
animais para a igreja.

19
Houve grande revolta. As crianças não aceitaram o pedido.
Os pais concordaram com elas, dizendo:
- Padre, ou levamos as nossas crianças e seus animais de
estimação, ou não iremos mais à missa.
O sacerdote suplicou:
- Por favor, não façam isso, pois estarei acabado.

20
No domingo, os fiéis foram chegando. Traziam crianças e
animais. À medida que iam entrando, o bispo franzia a testa,
arregalava os olhos e perguntava:

- Padre, o que significa isso?
- Eu posso explicar. É que hoje celebraremos o dia de
São Francisco.
- São Francisco... Em junho?!
- E não é?!

21
- Padre, o senhor enlouqueceu?!
- Será que me enganei tanto assim?! Mas já que o erro foi
meu, vamos permitir que os animais fiquem; afinal, o que
pensará São Francisco? É ou não é, senhor bispo?
O bispo aceitou a situação, e a missa foi celebrada.
Ao terminar a cerimônia, a família de Cloé convidou o
padre e o bispo para almoçar. A visita se estendeu até o
jantar.

22

No jantar, foram servidos ovos cozidos. De repente,
alguém exclamou:
- Encontrei!
- O quê? - perguntou a mãe.
- O anel.
Todos falaram ao mesmo tempo:
- O aneeeel?!
- Eu também mastiguei alguma coisa! - disse o pai.
- Será que o bendito anel está se desmanchando!? – falou o
padre - Até que enfim, acabou o pesadelo!

23

O bispo, que desconhecia a história do anel, ficou curioso.
Também tinha mastigado algo que quase lhe quebrara os
dentes.
Ao examinar, viu que não era o anel, mas sim, uma
pequenina pérola.
Suspirando, pensou:
- É milagre! Só pode ser!
E guardou-a no bolso.

24

O fenômeno passou a se repetir em cada ovo que
Branquinha botava.

A família, para protegê-la contra a inveja das pessoas,
manteve segredo. Marli ganhou outro anel, tão bonito
quanto o primeiro.

25

Cloé e Marli cresceram, mas o mistério do anel de pérola
continuou. Muitos bruxos tentaram descobrir o que
acontecera, mas nenhum deles conseguiu.
Quanto à Branquinha... Bem,
ela teve bastantes pintinhos e
viveu feliz, cacarejando, toda
animada. Alguns dizem que
era encantada, pois nenhuma
outra galinha viveu tanto.
Mas a história não termina aí,
não.

Uma tarde, Cloé estava sentada no mesmo lugar onde todo
o mistério havia começado. De repente, alguma coisa
chamou-lhe a atenção. Numa pequena fresta, existente ao
pé da escada, avistou o anel há muito tempo perdido. A
alegria foi geral, e todos foram felizes enquanto viveram.

Olá,
Sou a Lenira Almeida Heck, mas muitos me
conhecem como Júlia Vehuiah. Sou professora, faço
palestras e gosto muito de escrever e contar
histórias. Nasci em 20/03/54, lá na cidade de São
Félix/BA; até os nove anos morei em Cachoeira/BA
e tomei muito banho no rio Paraguaçu. Depois
mudamos para Salvador/BA. Atualmente moro em
Lajeado/RS, terra de gente maravilhosa! Vocês
precisam conhecer a cidade e o povo. Sou casada
com Roque Heck e sou mãe de Aline, Davi e de uma
gata de quatro patas, sem pedigree, chamada Mini,
sapeca que só ela.
Gosto das coisas simples, como, por exemplo, um belo dia de sol
após um lindo dia de chuva; dos animais; da algazarra de crianças quando elas
estão felizes.
Adoro a Deus e sei que sou amada por ELE.
A todos vocês, obrigada por ler as nossas obras e até um dia,
quando nos veremos. Um beijo bem gordo na bochecha.
Dersoy:

Quero falar um pouco sobre a nossa ilustradora Adriana Schnor

Fomos colegas no magistério. Ela é casada, tem dois filhos e
nasceu em Santa Clara do Sul/RS. Hoje, atua na Educação Infantil. Desde
criança, sempre gostou de desenhar e pintar, e tem o potencial artístico que
vocês já conhecem. Adriana é muito mais, mas não tenho espaço para
escrever tudo.
Amo vocês.
Um grande abraço, da Lenira

Aquisição das obras:
Lenira Almeida Heck:
Fone: (51)3714-2472
E-mail: lenira@univates.br

Vamos pintar os personagens das outras histórias?

Borboleta Azul

Galo Tião

Peixinho Vermelho

Dinda Raposa

Vaca Malhada

Sr. Gato

Outras obras da Autora:

Entre
nessa
corrente!

Um anel sumiu misteriosamente. A galinha Branquinha é a
principal suspeita, por isso é perseguida;

mas ela tem como defensora a sua pequena dona,
Cloé. Venha participar dessa fantástica e
divertida aventura.

ISBN 978-85-98611-54-9

9 788598 611549

