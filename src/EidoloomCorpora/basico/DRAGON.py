import random
import time


def displayIntro():
    print('Você está em uma terra cheia de dragões. Na frente de você,')
    print('você vê duas cavernas. Em uma caverna, o dragão é amigo')
    print('e compartilhará seu tesouro com você. O outro dragão')
    print('é ganancioso e com fome, e vai comê-lo.')
    print()


def chooseCave():
    cave = ''
    while cave != '1' and cave != '2':
        print('Em qual caverna você vai entrar? (1 or 2)')
        cave = input()
    return cave


def checkCave(chosenCave):
    print('você se aproxima da caverna...')
    time.sleep(2)
    print('É escuro e assustador...')
    time.sleep(2)
    print('Um grande dragão pula na sua frente! Ele abre as mandíbulas e  ...')
    print()
    time.sleep(2)
    friendlyCave = random.randint(1, 2)

    if chosenCave == str(friendlyCave):
        print('Lhe dara o seu tesouro!')
    else:
        print('Devora você em um piscar de olhos!')


playAgain = 'Sim'
while playAgain == 'Sim' or playAgain == 'S':
    displayIntro()
    caveNumber = chooseCave()
    checkCave(caveNumber)
    print('Você quer jogar de novo? (Sim or não)')
    playAgain = input()
