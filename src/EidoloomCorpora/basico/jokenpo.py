# Faça o computador jogar Jokempô com você
from random import choice
from time import sleep

print('='*52)
print('{:^52}'.format('QUERO VER VOCÊ ME GANHAR NO JOKENPO'))
print('='*52)

print('Escolha Pedra, Papel ou Tesoura')
eu = str(input('Jogada: ')).lower()

lista = ['pedra','papel','tesoura']
comp = choice(lista)

if eu == comp:
    status = 'EMPATE'
elif eu != comp:
    if eu == 'pedra':
        if comp == 'tesoura':
            status = 'VOCÊ GANHOU'
        if comp == 'papel':
            status = 'O COMPUTADOR GANHOU'
    if eu == 'tesoura':
        if comp == 'papel':
            status = 'VOCÊ GANHOU'
        if comp == 'pedra':
            status = 'O COMPUTADOR GANHOU'
    if eu == 'papel':
        if comp == 'pedra':
            status = 'VOCÊ GANHOU'
        if comp == 'tesoura':
            status = 'O COMPUTADOR GANHOU'

print('\n{:^52}'.format('JO'))
sleep(1)
print('{:^52}'.format('  KEN'))
sleep(1)
print('{:^52}'.format('   PO !!'))
sleep(1)
print("\n{:=^52}\nNessa partida ELE jogou {} e VOCÊ jogou {}".format(status, comp, eu))