import os

TXT_DIR: str = os.path.dirname(os.path.realpath(__file__))

print(TXT_DIR)

for filepath in os.listdir(TXT_DIR):
    fpath = os.path.join(TXT_DIR, filepath)
    if (os.path.isdir(fpath)):
        for gamefilepath in os.listdir(fpath):
            with open(os.path.join(fpath, gamefilepath)) as gamefile:
                print(gamefile.read())