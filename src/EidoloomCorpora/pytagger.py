import ast
import os

tree = ast.parse('''
from random import choice
if eu == comp:
    status = 'EMPATE' 
''')
# print(ast.dump(tree))

# from spekuloom.util import Mont
# d = Mont().mont_symbol_pt()
# for k, v in d.items():
#     print(k, v)

TOKENS = dict(
    Module='▲', Assign='●', Name='△', Store='▣', List='▣', Str='◱', Load='◬', ClassDef='◭', FunctionDef='◭',
    arguments='▵', arg='▵', Expr='◉', ListComp='◈', comprehension='◈', Call='◲', Num='◳', For='◠', Attribute='◨',
    Import='●', alias='◡', ImportFrom='◠', Dict='▣', With='◡', ExceptHandler='◔', Raise='●', Pass='●', Return='●',
    Global='◭', Nonlocal='◭', IfExp='▬', Compare='◎', Gt='▱', UnaryOp='◉', USub='▲', While='◡', In='○', AugAssign='●',
    Add='■', If='▬', Continue='●', GtE='▰', Break='●', Is='○', NameConstant='▲', DictComp='◈', JoinedStr='◲',
    FormattedValue='◲', Tuple='▣', GeneratorExp='◈', Yield='●', BoolOp='◻', Or='▬', Lt='▱', And='▬', Eq='◻', LtE='▰',
    Not='▬', Assert='●', Delete='◕', Del='●', Lambda='◪', BinOp='■', Mult='■', Pow='■', Sub='■', Div='■',
    FloorDiv='■', Mod='■'
)


class GenV(ast.NodeVisitor):
    def __init__(self):
        self.tokens = {}
        self.gram = ""

    def generic_visit(self, node):
        token_name = type(node).__name__
        self.tokens[token_name] = "\033[1;30m" + u"\u25B2" + "\033[1;0m"
        if token_name in TOKENS:
            self.tokens[token_name] = TOKENS[token_name]
            self.gram += TOKENS[token_name]
        ast.NodeVisitor.generic_visit(self, node)

    def tokenize(self, text):
        self.visit(ast.parse(text))
        return list(self.gram)


class NodeVisitor(ast.NodeVisitor):
    def visit_Call(self, tree_node):
        print('◲{}'.format(tree_node.func.id))
        self.visit(tree_node.func)

    def visit_Str(self, tree_node):
        print('◱{}'.format(tree_node.s))

    def visit_Name(self, tree_node):
        print('△{}'.format(tree_node.id))

    def visit_Assign(self, tree_node):
        print('●{}'.format(list(tree_node.targets)[0].id))
        self.visit(tree_node.value)

    def visit_For(self, tree_node):
        print('◠{}'.format(tree_node.target.id))
        [self.visit(node) for node in tree_node.body]

    def visit_Attribute(self, tree_node):
        print('◨{}'.format(tree_node.value.s))
        [self.visit(node) for node in tree_node.args]

    def visit_ClassDef(self, tree_node):
        print('◭{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_FunctionDef(self, tree_node):
        print('◭{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Module(self, tree_node):
        print('▲{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Store(self, tree_node):
        print('▣{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_List(self, tree_node):
        print('▣{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Str(self, tree_node):
        print('◱{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_arguments(self, tree_node):
        print('▵{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_arg(self, tree_node):
        print('▵{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Expr(self, tree_node):
        print('◉{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_ListComp(self, tree_node):
        print('◈{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_comprehension(self, tree_node):
        print('◈{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Num(self, tree_node):
        print('◳{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Import(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_alias(self, tree_node):
        print('◡{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_ImportFrom(self, tree_node):
        print('◠{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Dict(self, tree_node):
        print('▣{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_With(self, tree_node):
        print('◡{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_ExceptHandler(self, tree_node):
        print('◔{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Raise(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Pass(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Return(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Global(self, tree_node):
        print('◭{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Nonlocal(self, tree_node):
        print('◭{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_IfExp(self, tree_node):
        print('▬{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Compare(self, tree_node):
        print('◎{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Gt(self, tree_node):
        print('▱{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_UnaryOp(self, tree_node):
        print('◉{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_USub(self, tree_node):
        print('▲{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_While(self, tree_node):
        print('◡{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_In(self, tree_node):
        print('○{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_AugAssign(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Add(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_If(self, tree_node):
        print('▬{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Continue(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_GtE(self, tree_node):
        print('▰{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Break(self, tree_node):
        print('▰{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Is(self, tree_node):
        print('○{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_NameConstant(self, tree_node):
        print('▲{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_DictComp(self, tree_node):
        print('◈{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_JoinedStr(self, tree_node):
        print('◲{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_FormattedValue(self, tree_node):
        print('◲{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Tuple(self, tree_node):
        print('▣{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_GeneratorExp(self, tree_node):
        print('◈{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_GeneratorExp(self, tree_node):
        print('◈{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Yield(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_BoolOp(self, tree_node):
        print('◻{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Or(self, tree_node):
        print('▬{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Lt(self, tree_node):
        print('▱{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_And(self, tree_node):
        print('▬{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Eq(self, tree_node):
        print('◻{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_LtE(self, tree_node):
        print('▰{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Not(self, tree_node):
        print('▬{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Assert(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Delete(self, tree_node):
        print('◕{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Del(self, tree_node):
        print('●{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Lambda(self, tree_node):
        print('◪{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_BinOp(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Mult(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Pow(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Sub(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Div(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_FlorDiv(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_Mod(self, tree_node):
        print('■{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]


if __name__ == "__main__":

    gv = GenV()
    gv.visit(tree)
    for tok in gv.tokens:
        print("{}='{}',".format(tok, gv.tokens[tok]), end=' ')
    print("toks")

    for tok in gv.gram:
        print("{}".format(tok), end='')

"""
encondes:
noun,▲  False True None▲

name △
numeral ◳
textual ◱
article,

adjective,◭
class def lambda◪
global nonlocal◭

verb, ●    
Basico:break pass◐ import◕ return●
Intermediário:continue, del  
Avançado:try raise◔ assert yeld 

adverb,○   finally except○
in is◎

preposition,◠  from with as◠ for◠

conjuction,▬  if else elif▬
or and not◻ while
"""
